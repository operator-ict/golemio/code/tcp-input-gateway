import { singleton, inject, injectAll } from "@golemio/core/dist/shared/tsyringe";
import { DomainToken, IAMQPService } from "#domain";
import { ReceiverToken, IReceiver } from "#receivers";
import { ITransportInputGatewayApp } from "./interfaces";

@singleton()
export class TransportInputGatewayApp implements ITransportInputGatewayApp {
    private readonly amqpService: IAMQPService;
    private readonly receivers: IReceiver[];

    constructor(
        @inject(DomainToken.AMQPService) amqpService: IAMQPService,
        @injectAll(ReceiverToken.Receiver) receivers: IReceiver[]
    ) {
        this.amqpService = amqpService;
        this.receivers = receivers;
    }

    /**
     * Initialize connections and start all receivers
     */
    public async start() {
        // Connections
        await this.amqpService.connect();

        // Start all receivers
        for (const receiver of this.receivers) {
            receiver.startListening();
        }
    }

    /**
     * Destroy connections and stop all receivers
     */
    public stop(exitCode?: number) {
        Promise.all([this.amqpService.disconnect(), ...this.receivers.map((receiver) => receiver.stopListening())]).then(
            (res) => {
                const errors = res.filter((val) => val instanceof Error);

                if (errors.length > 0) {
                    process.exit(exitCode ?? 1);
                } else {
                    process.exit(exitCode ?? 0);
                }
            }
        );

        setTimeout(() => {
            process.exit(2);
        }, 5000);
    }
}
