export interface ITransportInputGatewayApp {
    start(): Promise<void>;
    stop(exitCode?: number): void;
}
