/* istanbul ignore file */
import "@golemio/core/dist/shared/_global";

// Load telemetry before all deps - instrumentation patches the libs on load
import { initTraceProvider } from "@golemio/core/dist/monitoring";
initTraceProvider();

import { ApplicationToken, ITransportInputGatewayApp, TransportInputGatewayApp } from "#application";
import { AMQPService, Config, DomainToken, ILoggerService, LoggerService } from "#domain";
import { ArrivaCityReceiver, DPPConfig, DPPMetroReceiver, DPPReceiverEnum, DPPReceiverFactory, ReceiverToken } from "#receivers";
import { XMLMessageFilter } from "#receivers/helpers/xml-message-filter.helper";
import { StorageManager } from "#receivers/managers";
import { DPPMetroParser, DPPTramBusParser, RegionalBusParser } from "#receivers/parsers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { TableStorageServiceFactory } from "@golemio/core/dist/helpers/data-access/table-storage/TableStorageServiceFactory";
import { TableStorageProvider } from "@golemio/core/dist/helpers/data-access/table-storage/providers/enums/TableStorageProviderEnum";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { container as commonContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";

const applicationContainer = commonContainer.createChildContainer();

// Framework registrations
commonContainer.registerInstance(CoreToken.SimpleConfig, new SimpleConfig({ env: process.env }));

// Domain registrations
commonContainer
    .register(DomainToken.Configuration, { useValue: Config })
    .registerSingleton(DomainToken.LoggerService, LoggerService)
    .registerSingleton(DomainToken.AMQPService, AMQPService)
    .register(DomainToken.TableStorageService, {
        useFactory: instanceCachingFactory((c) => {
            const config = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
            const serviceFactory = new TableStorageServiceFactory(
                {
                    enabled: config.getBoolean("env.TABLE_STORAGE_ENABLED", false),
                    provider: {
                        [TableStorageProvider.Azure]: {
                            clientId: config.getValue("env.AZURE_TABLE_CLIENT_ID", ""),
                            tenantId: config.getValue("env.AZURE_TABLE_TENANT_ID", ""),
                            clientSecret: config.getValue("env.AZURE_TABLE_CLIENT_SECRET", ""),
                            account: config.getValue("env.AZURE_TABLE_ACCOUNT", ""),
                            entityBatchSize: Number.parseInt(config.getValue("env.AZURE_TABLE_STORAGE_ENTITY_BATCH_SIZE", "100")),
                        },
                    },
                },
                c.resolve<ILoggerService>(DomainToken.LoggerService).getLogger()
            );

            return serviceFactory.getService(TableStorageProvider.Azure);
        }),
    });

// Receiver registrations
applicationContainer
    .register(ReceiverToken.DPPConfiguration, { useValue: DPPConfig })
    .register(ReceiverToken.StorageManager, StorageManager)
    .registerSingleton(ReceiverToken.DPPTramBusParser, DPPTramBusParser)
    .registerSingleton(ReceiverToken.DPPMetroParser, DPPMetroParser)
    .registerSingleton(ReceiverToken.RegionalBusParser, RegionalBusParser)
    .registerSingleton(ReceiverToken.XMLMessageFilter, XMLMessageFilter)
    .registerSingleton(ReceiverToken.Receiver, DPPReceiverFactory.getClass(DPPReceiverEnum.Tram))
    .registerSingleton(ReceiverToken.Receiver, DPPReceiverFactory.getClass(DPPReceiverEnum.Bus))
    .registerSingleton(ReceiverToken.Receiver, DPPMetroReceiver)
    .registerSingleton(ReceiverToken.Receiver, ArrivaCityReceiver);

// Application registrations
applicationContainer.registerSingleton(ApplicationToken.TransportInputGatewayApp, TransportInputGatewayApp);

// Resolve application instance and start the app
const app = applicationContainer.resolve<ITransportInputGatewayApp>(ApplicationToken.TransportInputGatewayApp);
app.start();

// Handle process signals
process.on("SIGTERM", () => app.stop());
process.on("SIGINT", () => app.stop());

for (const event of ["uncaughtException", "unhandledRejection"]) {
    const log = commonContainer.resolve<ILoggerService>(DomainToken.LoggerService).getLogger();

    process.on(event as NodeJS.Signals, (err) => {
        // Log error, gracefully shut down and exit with exit code 4
        log.error(err);
        app.stop(4);
    });
}
