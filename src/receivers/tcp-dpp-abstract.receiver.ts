import { IAMQPService } from "#domain";
import { IMessageFilter, IMessageParser, IStorageManager, TCPSocket } from "#receivers";
import { MessageBuffer } from "./buffers/message.buffer";
import { IReceiverConfiguration } from "./config/interfaces/IReceiverConfiguration";
import { extend } from "./helpers";
import { AbstractTCPReceiver } from "./tcp-abstract.receiver";

export abstract class AbstractDPPReceiver extends AbstractTCPReceiver {
    protected abstract readonly amqpService: IAMQPService;
    protected abstract readonly storageManager: IStorageManager;
    protected abstract readonly messageFilter: IMessageFilter;
    protected abstract readonly messageParser: IMessageParser;
    protected abstract readonly receiverConfig: IReceiverConfiguration;
    protected abstract readonly amqpMessageKey: string;

    /**
     * Extend configureSocket from AbstractTCPReceiver
     */
    @extend()
    protected configureSocket(socket: TCPSocket) {
        socket.readBuffer = new MessageBuffer("</M>");
    }

    /**
     * Extend handleSocketData from AbstractTCPReceiver
     */
    @extend()
    protected async handleSocketData(socket: TCPSocket, data: Buffer) {
        socket.readBuffer!.push(data);
        if (socket.readBuffer!.isFinished()) {
            const message = socket.readBuffer!.getMessage();
            if (!message) {
                this.log.error(`[${this.constructor.name}] Received empty message: buffer[${data}]`);
                return;
            }

            return this.processMessage(message);
        }
    }

    /**
     * Extend handleSocketTimeout from AbstractTCPReceiver
     */
    @extend()
    protected async handleSocketTimeout(socket: TCPSocket) {
        const message = socket.readBuffer?.buffer;
        if (message) {
            return this.processMessage(message);
        }
    }

    /**
     * Parse message and forward it to exchange
     */
    private async processMessage(message: string) {
        if (this.messageFilter.shouldBeIgnored(message)) return;

        const payload = await this.messageParser.parseData(message);
        if (!payload) {
            return;
        }

        this.saveDataToStorage(payload, message);

        this.log.debug(payload);
        return this.amqpService.sendMessageToExchange(this.amqpMessageKey, JSON.stringify(payload), {
            persistent: true,
            timestamp: Date.now(),
        });
    }
}
