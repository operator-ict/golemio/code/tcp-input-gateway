import { DomainToken, IAMQPService, IConfiguration, ILoggerService } from "#domain";
import { IDPPConfiguration, IMessageFilter, IMessageParser, IStorageManager, ReceiverToken } from "#receivers";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IReceiverConfiguration } from "./config/interfaces/IReceiverConfiguration";
import { AbstractDPPReceiver } from "./tcp-dpp-abstract.receiver";

export enum DPPReceiverEnum {
    Tram,
    Bus,
}

export class DPPReceiverFactory {
    public static getClass = (receiver: DPPReceiverEnum) => {
        let receiverName: string;

        switch (receiver) {
            case DPPReceiverEnum.Tram:
                receiverName = "DPPTramReceiver";
                break;

            case DPPReceiverEnum.Bus:
                receiverName = "DPPBusReceiver";
        }

        @injectable()
        class DPPReceiver extends AbstractDPPReceiver {
            protected readonly amqpService;
            protected readonly storageManager;
            protected readonly messageFilter;
            protected readonly messageParser;
            protected readonly receiverConfig;
            protected readonly amqpMessageKey;

            constructor(
                @inject(DomainToken.Configuration) config: IConfiguration,
                @inject(DomainToken.LoggerService) loggerService: ILoggerService,
                @inject(DomainToken.AMQPService) amqpService: IAMQPService,
                @inject(ReceiverToken.DPPConfiguration) dppConfig: IDPPConfiguration,
                @inject(ReceiverToken.StorageManager) storageManager: IStorageManager,
                @inject(ReceiverToken.XMLMessageFilter) messageFilter: IMessageFilter,
                @inject(ReceiverToken.DPPTramBusParser) messageParser: IMessageParser
            ) {
                let receiverConfig: IReceiverConfiguration, workerMethod: string;
                switch (receiver) {
                    case DPPReceiverEnum.Tram:
                        receiverConfig = dppConfig.tcp.tram;
                        workerMethod = "saveTramRunsToDB";
                        break;

                    case DPPReceiverEnum.Bus:
                        receiverConfig = dppConfig.tcp.bus;
                        workerMethod = "saveBusRunsToDB";
                }

                super(receiverConfig.port, receiverConfig.bufferSizeLimit, loggerService.getLogger());
                this.amqpService = amqpService;
                this.storageManager = storageManager;
                this.messageFilter = messageFilter;
                this.messageParser = messageParser;
                this.receiverConfig = receiverConfig;
                this.amqpMessageKey = `input-transport.${config.rabbit.exchangeName}.vehiclepositionsruns.${workerMethod}`;
            }

            protected saveDataToStorage(payload: Record<string, any>, message: string): void {
                if (!payload.M?.V) {
                    return;
                }

                const inputData = [
                    {
                        $: {
                            raw: message,
                        },
                    },
                    ...(Array.isArray(payload.M.V) ? payload.M.V : [payload.M.V]),
                ];

                this.storageManager.saveData(this.receiverConfig.storageTableName, inputData);
            }
        }

        // Set receiver name
        Object.defineProperty(DPPReceiver, "name", { value: receiverName });
        return DPPReceiver;
    };
}
