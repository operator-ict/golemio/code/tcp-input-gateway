export const ReceiverToken = {
    Receiver: Symbol(),
    StorageManager: Symbol(),
    DPPConfiguration: Symbol(),
    DPPMetroParser: Symbol(),
    DPPTramBusParser: Symbol(),
    RegionalBusParser: Symbol(),
    XMLMessageFilter: Symbol(),
};
