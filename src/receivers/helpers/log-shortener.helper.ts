export class LogShortener {
    /**
     * Truncate a given log message if it exceeds certain maximum length
     *
     * @param message The message to truncate
     * @param maxLength The maximum length of the message
     */
    public static truncate(message: string, maxLength = 130): string {
        if (message.length <= maxLength) return message;
        const truncatedInfo = "... (truncated)";
        const truncatedMessage = message.slice(0, Math.max(maxLength - truncatedInfo.length, 0));
        return `${truncatedMessage}${truncatedInfo}`;
    }
}
