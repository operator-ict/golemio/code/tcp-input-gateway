import { IMessageFilter } from "#receivers/interfaces";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class XMLMessageFilter implements IMessageFilter {
    /**
     * Returns `true` if a given message should be ignored and not processed further
     *
     * @param message The message to check
     */
    shouldBeIgnored(message: string): boolean {
        const trimmedMessage = message.trim();
        if (trimmedMessage.length === 0) {
            return true;
        }
        if (!trimmedMessage.startsWith("<") || !trimmedMessage.endsWith(">")) {
            return true;
        }
        return false;
    }
}
