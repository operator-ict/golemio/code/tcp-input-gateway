export interface IRunsMessageProperties {
    turnus?: string;
    line?: string;
    evc?: string;
    np?: string;
    lat?: string;
    lng?: string;
    akt?: string;
    takt?: string;
    konc?: string;
    tjr?: string;
    pkt?: string;
    tm?: string;
    events?: string;
}

export interface IRunsInput {
    M?: IRunsMessageContent;
}

export interface IRunsMessageContent {
    V?: IRunsMessagePropertiesWrapper | IRunsMessagePropertiesWrapper[];
}

export interface IRunsMessagePropertiesWrapper {
    $?: IRunsMessageProperties;
}
