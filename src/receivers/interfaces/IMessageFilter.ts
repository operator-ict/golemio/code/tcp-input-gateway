export interface IMessageFilter {
    shouldBeIgnored(message: string): boolean;
}
