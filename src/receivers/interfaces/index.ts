import net from "net";
import { MessageBuffer } from "#receivers/buffers/message.buffer";

export interface IReceiver {
    startListening(): void;
    stopListening(): Promise<Error | null>;
}

export interface IStorageManager {
    saveData(tableName: string, vehicleData: object[], messageData?: object): Promise<void>;
}

export class TCPSocket extends net.Socket {
    id?: string;
    readBuffer?: MessageBuffer;
}

export interface IActiveTCPClient {
    socket: TCPSocket;
    lastMessage?: Buffer;
}

export type ActiveTCPClients = Map<string, IActiveTCPClient>;

export * from "./IMessageFilter";
export * from "./IMessageParser";
