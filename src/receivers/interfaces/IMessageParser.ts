export interface IMessageParser {
    parseData(data: string): Promise<object | undefined>;
}
