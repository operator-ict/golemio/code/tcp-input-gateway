import { DomainToken, ILoggerService, LogObject } from "#domain";
import { IStorageManager } from "#receivers/interfaces";
import { ITableStorageService } from "@golemio/core/dist/helpers/data-access/table-storage/providers/interfaces/ITableStorageService";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class StorageManager implements IStorageManager {
    private readonly logger: LogObject;

    constructor(
        @inject(DomainToken.TableStorageService) private storageService: ITableStorageService,
        @inject(DomainToken.LoggerService) loggerService: ILoggerService
    ) {
        this.logger = loggerService.getLogger();
    }

    public async saveData(tableName: string, inputData: object[], messageData: object = {}): Promise<void> {
        let entites: object[] = [];

        for (const inputItem of inputData) {
            const entity = {
                ...messageData,
                ...("$" in inputItem ? (inputItem.$ as object) : {}),
            };

            entites.push(entity);
        }

        try {
            return await this.storageService.createEntities(tableName, entites);
        } catch (err) {
            this.logger.error(new GeneralError(`Error saving data to table storage: ${tableName}`, this.constructor.name, err));
        }
    }
}
