import { DomainToken, IAMQPService, IConfiguration, ILoggerService } from "#domain";
import { IMessageFilter, IMessageParser, IStorageManager, ReceiverToken, TCPSocket } from "#receivers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MessageBuffer } from "./buffers/message.buffer";
import { IReceiverConfiguration } from "./config/interfaces/IReceiverConfiguration";
import { extend } from "./helpers";
import { AbstractTCPReceiver } from "./tcp-abstract.receiver";

@injectable()
export class ArrivaCityReceiver extends AbstractTCPReceiver {
    private readonly receiverConfig: IReceiverConfiguration;
    private readonly amqpMessageKey: string;

    constructor(
        @inject(DomainToken.Configuration) config: IConfiguration,
        @inject(CoreToken.SimpleConfig) simpleConfig: ISimpleConfig,
        @inject(DomainToken.LoggerService) loggerService: ILoggerService,
        @inject(DomainToken.AMQPService) private amqpService: IAMQPService,
        @inject(ReceiverToken.StorageManager) private storageManager: IStorageManager,
        @inject(ReceiverToken.XMLMessageFilter) private messageFilter: IMessageFilter,
        @inject(ReceiverToken.RegionalBusParser) private messageParser: IMessageParser
    ) {
        const bufferSizeLimit = simpleConfig.getValue<string>("env.ARRIVA_CITY_BUFFER_SIZE_LIMIT", "2");
        const receiverConfig = {
            port: simpleConfig.getValue<string>("env.ARRIVA_CITY_RECEIVER_PORT", "3005"),
            bufferSizeLimit: Number.parseFloat(bufferSizeLimit),
            storageTableName: simpleConfig.getValue<string>("env.ARRIVA_CITY_STORAGE_TABLE_NAME", "TcpArrivaCityData"),
        };

        super(receiverConfig.port, receiverConfig.bufferSizeLimit, loggerService.getLogger());
        this.receiverConfig = receiverConfig;
        this.amqpMessageKey = `input-transport.${config.rabbit.exchangeName}.vehiclepositionsruns.saveArrivaCityRunsToDB`;
    }

    @extend()
    protected configureSocket(socket: TCPSocket) {
        socket.readBuffer = new MessageBuffer("</M>");
    }

    @extend()
    protected async handleSocketData(socket: TCPSocket, data: Buffer) {
        socket.readBuffer!.push(data);
        if (socket.readBuffer!.isFinished()) {
            const message = socket.readBuffer!.getMessage();
            if (!message) {
                this.log.error(`[${this.constructor.name}] Received empty message: buffer[${data}]`);
                return;
            }

            return this.processMessage(message);
        }
    }

    @extend()
    protected async handleSocketTimeout(socket: TCPSocket) {
        const message = socket.readBuffer?.buffer;
        if (message) {
            return this.processMessage(message);
        }
    }

    protected saveDataToStorage(payload: Record<string, any>, message: string): void {
        if (!payload.M?.V) {
            return;
        }

        const inputData = [
            {
                $: {
                    raw: message,
                },
            },
            ...(Array.isArray(payload.M.V) ? payload.M.V : [payload.M.V]),
        ];

        this.storageManager.saveData(this.receiverConfig.storageTableName, inputData);
    }

    private async processMessage(message: string) {
        if (this.messageFilter.shouldBeIgnored(message)) return;

        const payload = await this.messageParser.parseData(message);
        if (!payload) {
            return;
        }

        this.saveDataToStorage(payload, message);

        this.log.debug(payload);
        return this.amqpService.sendMessageToExchange(this.amqpMessageKey, JSON.stringify(payload), {
            persistent: true,
            timestamp: new Date().getTime(),
        });
    }
}
