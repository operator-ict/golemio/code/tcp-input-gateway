import { DomainToken, ILoggerService, LogObject } from "#domain";
import { LogShortener } from "#receivers/helpers/log-shortener.helper";
import { IRunsInput, IRunsMessageProperties, IRunsMessagePropertiesWrapper } from "#receivers/interfaces/IRunsMessageInterface";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractXMLParser } from "./abstract-xml.parser";

@injectable()
export class DPPTramBusParser extends AbstractXMLParser {
    /** @example <V turnus="134/3" line="134" evc="3738" events="O" ... /> */
    private static REGEXP_VALID_CONTENT_XML = /(<V[^>]+>)/g;

    private readonly log: LogObject;

    constructor(@inject(DomainToken.LoggerService) loggerService: ILoggerService) {
        super();
        this.log = loggerService.getLogger();
    }

    /**
     * Parse xml data to json string
     */
    public async parseData(data: string): Promise<object | undefined> {
        let str = "";
        try {
            str = this.normalizeData(data);
            const msg = await this.parseXml(str);
            if (msg) {
                return this.removeInvalidRecords(msg);
            }
            this.log.warn(`[${this.constructor.name}] Received empty message: buffer[${Buffer.from(data)}]`);
        } catch (err) {
            this.log.error(
                new GeneralError(
                    LogShortener.truncate(`Error parsing received data. Attempting recovery from: ${data}`),
                    this.constructor.name,
                    err
                )
            );
            const recoverData = await this.recoverData(str);
            if (recoverData) {
                return this.removeInvalidRecords(recoverData);
            }
        }
    }

    /**
     * Salvage data from invalid xml
     */
    private async recoverData(str: string): Promise<object | undefined> {
        try {
            const validStrXml = str.match(DPPTramBusParser.REGEXP_VALID_CONTENT_XML);
            if (!validStrXml) {
                this.log.warn(`[${this.constructor.name}] Could not recover partial data`);
                return;
            }

            const msg = await this.parseXml(`<M>${validStrXml.join("")}</M>`);
            if (msg) {
                this.log.info(`[${this.constructor.name}] Data recovery successful`);
                return msg;
            }
        } catch (err) {
            this.log.error(
                new GeneralError(LogShortener.truncate(`Error recovering received data: ${str}`), this.constructor.name, err)
            );
        }
    }

    private removeInvalidRecords(data: IRunsInput): IRunsInput | undefined {
        if (data.M && data.M.V) {
            if (!Array.isArray(data.M.V)) {
                if (data.M.V.$) {
                    if (this.isRecordValid(data.M.V.$)) {
                        return data;
                    } else {
                        this.log.warn("Message for DPP tram/bus was filtered, count 1");
                    }
                }
            } else {
                const filterData = data.M.V.filter((element: IRunsMessagePropertiesWrapper) => {
                    return element.$ ? this.isRecordValid(element.$) : false;
                });
                if (filterData.length > 0) {
                    return { M: { V: filterData } };
                } else {
                    this.log.warn(`Message for DPP tram/bus was filtered, count ${data.M.V.length - filterData.length}`);
                }
            }
        }
    }
    private isRecordValid(element: IRunsMessageProperties): Boolean {
        const iso8601 =
            // eslint-disable-next-line max-len
            /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-3])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/; // same as above, except with a strict 'T' separator between date and time

        return (
            typeof element.lat === "string" &&
            typeof element.lng === "string" &&
            (element.takt ? iso8601.test(element.takt) : false)
        );
    }
}
