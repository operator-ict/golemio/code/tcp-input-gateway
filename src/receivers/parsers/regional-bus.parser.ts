import { DomainToken, ILoggerService, LogObject } from "#domain";
import { LogShortener } from "#receivers/helpers/log-shortener.helper";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractXMLParser } from "./abstract-xml.parser";

/**
 * Parse data from regional bus (non-DPP) providers' systems (such as ARRIVA City - TELMAX)
 * Can be shared between several receivers
 */
@injectable()
export class RegionalBusParser extends AbstractXMLParser {
    /** @example <V imei="867377023812559" rz="5T77105" pkt="767" ... /> */
    private static REGEXP_VALID_CONTENT_XML = /(<V[^>]+>)/g;
    private readonly log: LogObject;

    constructor(@inject(DomainToken.LoggerService) loggerService: ILoggerService) {
        super();
        this.log = loggerService.getLogger();
    }

    /**
     * Parse xml data to json string
     */
    public async parseData(data: string): Promise<object | undefined> {
        let str = "";
        try {
            str = this.normalizeData(data);
            const msg = await this.parseXml(str);

            if (!msg) {
                this.log.warn(`[${this.constructor.name}] Received empty message: buffer[${Buffer.from(data)}]`);
                return;
            }

            if (!Object.keys(msg).includes("M")) {
                this.log.warn(`[${this.constructor.name}] Received invalid message`);
                return;
            }

            return msg;
        } catch (err) {
            this.log.error(
                new GeneralError(
                    LogShortener.truncate(`Error parsing received data. Attempting recovery from: ${data}`),
                    this.constructor.name,
                    err
                )
            );
            return this.recoverData(str);
        }
    }

    /**
     * Salvage data from invalid xml
     */
    private async recoverData(str: string): Promise<object | undefined> {
        try {
            const validStrXml = str.match(RegionalBusParser.REGEXP_VALID_CONTENT_XML);
            if (!validStrXml) {
                this.log.warn(`[${this.constructor.name}] Could not recover partial data`);
                return;
            }

            const msg = await this.parseXml(`<M>${validStrXml.join("")}</M>`);
            if (msg) {
                this.log.info(`[${this.constructor.name}] Data recovery successful`);
                return msg;
            }
        } catch (err) {
            this.log.error(
                new GeneralError(LogShortener.truncate(`Error recovering received data: ${str}`), this.constructor.name, err)
            );
        }
    }
}
