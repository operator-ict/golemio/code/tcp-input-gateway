import { DomainToken, ILoggerService, LogObject } from "#domain";
import { LogShortener } from "#receivers/helpers/log-shortener.helper";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractXMLParser } from "./abstract-xml.parser";

@injectable()
export class DPPMetroParser extends AbstractXMLParser {
    /** @example <m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"> */
    private static REGEXP_MESSAGE_OPENING_TAG_XML = /^(<m[^>]+>)/;
    /** @example <vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /> */
    private static REGEXP_VALID_TRAINS_XML = /(<vlak[^>]+>)/g;

    private readonly log: LogObject;

    constructor(@inject(DomainToken.LoggerService) loggerService: ILoggerService) {
        super();
        this.log = loggerService.getLogger();
    }

    /**
     * Parse xml data to json string
     */
    public async parseData(data: string): Promise<object | undefined> {
        let str = "";
        try {
            str = this.normalizeData(data);
            const msg = await this.parseXml(str);
            if (!msg) {
                this.log.warn(`[${this.constructor.name}] Received empty message: buffer[${Buffer.from(data)}]`);
                return;
            }

            if (Object.keys(msg).toString() !== ["m"].toString()) {
                this.log.warn(`[${this.constructor.name}] Received invalid message`);
                return;
            }

            return msg;
        } catch (err) {
            this.log.error(
                new GeneralError(
                    LogShortener.truncate(`Error parsing received data. Attempting recovery from: ${data}`),
                    this.constructor.name,
                    err
                )
            );
            return this.recoverData(str);
        }
    }

    /**
     * Salvage data from invalid xml
     */
    private async recoverData(str: string): Promise<object | undefined> {
        try {
            const messageOpeningTagXml = str.match(DPPMetroParser.REGEXP_MESSAGE_OPENING_TAG_XML);
            const validTrainsXml = str.match(DPPMetroParser.REGEXP_VALID_TRAINS_XML);
            if (!messageOpeningTagXml || !validTrainsXml) {
                this.log.warn(`[${this.constructor.name}] Could not recover partial data`);
                return;
            }

            const msg = await this.parseXml(`${messageOpeningTagXml[0]}${validTrainsXml.join("")}</m>`);
            if (msg) {
                this.log.info(`[${this.constructor.name}] Data recovery successful`);
                return msg;
            }
        } catch (err) {
            this.log.error(
                new GeneralError(LogShortener.truncate(`Error recovering received data: ${str}`), this.constructor.name, err)
            );
        }
    }
}
