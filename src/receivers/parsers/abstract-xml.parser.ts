import { IMessageParser } from "#receivers/interfaces";
import { parseStringPromise as xmlParser } from "xml2js";

export abstract class AbstractXMLParser implements IMessageParser {
    private static REGEXP_NORMALIZED_DATA = /[\u0000-\u001F\u007F-\u009F]/g;

    abstract parseData(data: string): Promise<object | undefined>;

    protected parseXml(str: string): Promise<object> {
        return xmlParser(str, { explicitArray: false, normalize: true, trim: true });
    }

    protected normalizeData(str: string) {
        return str.replace(AbstractXMLParser.REGEXP_NORMALIZED_DATA, "");
    }
}
