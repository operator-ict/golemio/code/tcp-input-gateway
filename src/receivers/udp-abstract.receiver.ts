import udp, { Socket as DatagramSocket, RemoteInfo } from "dgram";
import { LogObject, LogLevel } from "#domain";
import { IReceiver } from "./interfaces";

export abstract class AbstractUDPReceiver implements IReceiver {
    protected readonly port: string;
    protected readonly bufferSizeLimit: number;
    protected readonly log: LogObject;
    protected readonly server: DatagramSocket;

    constructor(port: string, bufferSizeLimit: number, logger: LogObject) {
        this.port = port;
        this.bufferSizeLimit = bufferSizeLimit;
        this.log = logger;

        this.server = udp.createSocket({ type: "udp4" });
        this.server.on("message", (data, remote) => this.handleSocketMessage(remote, data));
        this.server.on("error", (err) => this.handleSocketError(err));
    }

    /**
     * Start the receiver
     */
    public startListening() {
        this.server.bind({ port: parseInt(this.port) });
        this.server.on("listening", () => {
            this.log.info(`[${this.constructor.name}] UDP socket is bound to port ${this.port}`);
        });
    }

    /**
     * Stop the receiver
     */
    public stopListening() {
        this.log.info(`[${this.constructor.name}] Closing the socket`);

        try {
            this.server.disconnect();
        } catch (err) {
            // For now we do not expect to send any data back to remote addresses
            // so in our case we consider the ERR_SOCKET_DGRAM_NOT_CONNECTED error code
            // to be the expected/default behavior
            if (err.code === "ERR_SOCKET_DGRAM_NOT_CONNECTED") {
                this.log.warn(`[${this.constructor.name}] Outbound connection is inactive`);
            } else {
                this.log.error(`[${this.constructor.name}] Error while disconnecting from remote`, err);
                return Promise.resolve(err);
            }
        }

        return new Promise<null>((resolve) => {
            this.server.close(async () => {
                this.log.info(`[${this.constructor.name}] Socket closed`);
                resolve(null);
            });
        });
    }

    /**
     * Log connection information
     */
    protected logSocketEvent(logLevel: LogLevel, message: string, rinfo?: RemoteInfo, data?: string) {
        this.log[logLevel]({
            receiver: this.constructor.name,
            eventMessage: message,
            ...(rinfo ? { remoteAddress: rinfo.address, remotePort: rinfo.port } : null),
            ...(data ? { data } : null),
        });
    }

    /**
     * Handle socket message event. This method should be extended in every subclass
     * See DPPMetroReceiver for an example
     */
    protected handleSocketMessage(rinfo: RemoteInfo, data: Buffer) {
        this.logSocketEvent("info", "Connection established", rinfo);

        // Set data size limit for one message
        if (rinfo.size > this.bufferSizeLimit * 1024 * 1024) {
            this.logSocketEvent("error", "Data size limit exceeded", rinfo);
            return;
        }

        this.logSocketEvent("silly", "Data received", rinfo, data.toString("utf8"));
    }

    /**
     * Handle socket error event. This method can be extended in a subclass
     * See DPPMetroReceiver for an example
     */
    protected handleSocketError(err: Error) {
        this.logSocketEvent("error", `Connection failure: ${err.message}`);
    }
}
