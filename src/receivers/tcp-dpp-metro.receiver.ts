import { DomainToken, IAMQPService, IConfiguration, ILoggerService } from "#domain";
import {
    AbstractDPPReceiver,
    IDPPConfiguration,
    IMessageFilter,
    IMessageParser,
    IStorageManager,
    ReceiverToken,
    TCPSocket,
} from "#receivers";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MessageBuffer } from "./buffers/message.buffer";
import { IReceiverConfiguration } from "./config/interfaces/IReceiverConfiguration";
import { extend } from "./helpers";

@injectable()
export class DPPMetroReceiver extends AbstractDPPReceiver {
    public readonly name = "DPPMetroReceiver";
    protected readonly amqpService: IAMQPService;
    protected readonly storageManager: IStorageManager;
    protected readonly messageFilter: IMessageFilter;
    protected readonly messageParser: IMessageParser;
    protected readonly receiverConfig: IReceiverConfiguration;
    protected readonly amqpMessageKey: string;

    constructor(
        @inject(DomainToken.Configuration) config: IConfiguration,
        @inject(DomainToken.LoggerService) loggerService: ILoggerService,
        @inject(DomainToken.AMQPService) amqpService: IAMQPService,
        @inject(ReceiverToken.DPPConfiguration) dppConfig: IDPPConfiguration,
        @inject(ReceiverToken.StorageManager) storageManager: IStorageManager,
        @inject(ReceiverToken.XMLMessageFilter) messageFilter: IMessageFilter,
        @inject(ReceiverToken.DPPMetroParser) messageParser: IMessageParser
    ) {
        const receiverConfig = dppConfig.tcp.metro;

        super(receiverConfig.port, receiverConfig.bufferSizeLimit, loggerService.getLogger());
        this.amqpService = amqpService;
        this.storageManager = storageManager;
        this.messageFilter = messageFilter;
        this.messageParser = messageParser;
        this.receiverConfig = receiverConfig;
        this.amqpMessageKey = `input-transport.${config.rabbit.exchangeName}.vehiclepositionsruns.saveMetroRunsToDB`;
    }

    @extend()
    protected configureSocket(socket: TCPSocket) {
        socket.readBuffer = new MessageBuffer("</m>");
    }

    protected saveDataToStorage(payload: Record<string, any>, message: string): void {
        if (!payload.m?.$ || !payload.m?.vlak) {
            return;
        }

        const inputData = [
            {
                $: {
                    raw: message,
                },
            },
            ...(Array.isArray(payload.m.vlak) ? payload.m.vlak : [payload.m.vlak]),
        ];

        this.storageManager.saveData(this.receiverConfig.storageTableName, inputData, payload.m.$);
    }
}
