export class MessageBuffer {
    private readonly msgEnd: string;
    private _buffer: string;

    constructor(msgEnd = "\n") {
        this._buffer = "";
        this.msgEnd = msgEnd;
    }

    get buffer() {
        return this._buffer;
    }

    private set buffer(data: string) {
        this._buffer = data;
    }

    public push(data: Buffer): void {
        this._buffer += data;
    }

    public isFinished(): boolean {
        return this.buffer.length === 0 || this.buffer.indexOf(this.msgEnd) !== -1;
    }

    public getMessage(): string | null {
        const delimiterIndex = this.buffer.indexOf(this.msgEnd);
        if (delimiterIndex !== -1) {
            const message = this.buffer.slice(0, delimiterIndex + this.msgEnd.length);
            this.buffer = this.buffer.replace(message, "");
            return message;
        }
        return null;
    }
}
