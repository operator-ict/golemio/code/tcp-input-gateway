import { LogLevel, LogObject } from "#domain";
import { nanoid } from "nanoid";
import net, { Server as TCPServer } from "net";
import { LogShortener } from "./helpers/log-shortener.helper";
import { ActiveTCPClients, IReceiver, TCPSocket } from "./interfaces";

export abstract class AbstractTCPReceiver implements IReceiver {
    protected readonly port: string;
    protected readonly bufferSizeLimit: number;
    protected readonly log: LogObject;
    protected readonly server: TCPServer;
    protected readonly clients: ActiveTCPClients = new Map();

    constructor(port: string, bufferSizeLimit: number, logger: LogObject) {
        this.port = port;
        this.bufferSizeLimit = bufferSizeLimit;
        this.log = logger;

        this.server = net.createServer();
        this.server.on("connection", (socket) => {
            this.logConnections();
            this.handleUpcomingConnection(socket);
        });

        this.server.on("error", (err) => {
            this.log.error("Server error", err);
        });
    }

    /**
     * Start the receiver
     */
    public startListening() {
        this.server.listen(this.port, () =>
            this.log.info(`[${this.constructor.name}] TCP server is listening on port ${this.port}`)
        );
    }

    /**
     * Stop the receiver
     */
    public stopListening() {
        if (!this.server.listening) {
            return Promise.resolve(null);
        }

        this.log.info(`[${this.constructor.name}] Stopping the server`);
        return new Promise<Error | null>((resolve) => {
            for (const client of this.clients.values()) {
                if (!client.socket.destroyed) {
                    client.socket.end(() => {
                        client.socket.destroy();
                    });
                }
            }

            this.server.close(async (err) => {
                this.server.unref();

                if (err) {
                    this.log.error(`[${this.constructor.name}] Error while stopping the server`, err);
                    resolve(err);
                } else {
                    this.log.info(`[${this.constructor.name}] Server stopped`);
                    resolve(null);
                }
            });
        });
    }

    protected abstract saveDataToStorage(payload: object, message: string): void;

    /**
     * Log connection/client information
     */
    protected logSocketEvent(logLevel: LogLevel, message: string, socket: TCPSocket, data?: string) {
        this.log[logLevel]({
            receiver: this.constructor.name,
            eventMessage: message,
            remoteAddress: socket.remoteAddress,
            remotePort: socket.remotePort,
            ...(socket.id ? { socketId: socket.id } : null),
            ...(socket.readBuffer ? { buffer: LogShortener.truncate(socket.readBuffer.buffer) } : null),
            ...(data ? { data } : null),
        });
    }

    /**
     * Handle socket data event. This method should be extended in every subclass
     * See AbstractDPPReceiver for an example
     */
    protected handleSocketData(socket: TCPSocket, data: Buffer) {
        // Set data size limit for one connection
        if (socket.bytesRead > this.bufferSizeLimit * 1024 * 1024) {
            this.logSocketEvent("error", "Data size limit exceeded", socket);
            socket.write(`NACK data rejected\r\n`);
            socket.destroy();

            return;
        }

        this.logSocketEvent("silly", "Data received", socket, data.toString("utf8"));
        this.clients.set(socket.id!, { socket, lastMessage: data });
    }

    /**
     * Handle socket end event. This method can be extended in a subclass
     * See AbstractDPPReceiver for an example
     */
    protected handleSocketEnd(socket: TCPSocket) {
        this.logSocketEvent("info", "Closing connection", socket);
    }

    /**
     * Handle socket close event. This method can be extended in a subclass
     * See AbstractDPPReceiver for an example
     */
    protected handleSocketClose(socket: TCPSocket) {
        this.clients.delete(socket.id!);
        this.logSocketEvent("info", "Connection closed", socket);
    }

    /**
     * Handle socket timeout event. This method can be extended in a subclass
     * See AbstractDPPReceiver for an example
     */
    protected handleSocketTimeout(socket: TCPSocket) {
        this.logSocketEvent("warn", "Connection timed out", socket, this.getLastClientMessage(socket));
        socket.destroy();
    }

    /**
     * Handle socket error event. This method can be extended in a subclass
     * See AbstractDPPReceiver for an example
     */
    protected handleSocketError(socket: TCPSocket, err: Error) {
        this.logSocketEvent("error", `Connection failure: ${err.message}`, socket, this.getLastClientMessage(socket));
    }

    /**
     * Set up socket event handlers
     */
    protected configureSocket(socket: TCPSocket) {
        // Configure socket
        socket.setTimeout(60000);
        socket.id = nanoid();
    }

    /**
     * Handle upcoming connection
     */
    private handleUpcomingConnection(socket: TCPSocket) {
        this.configureSocket(socket);

        this.logSocketEvent("info", "Connection established", socket);

        this.clients.set(socket.id!, { socket });

        socket.on("data", (data) => this.handleSocketData(socket, data));
        socket.on("end", () => this.handleSocketEnd(socket));
        socket.on("close", () => this.handleSocketClose(socket));
        socket.on("timeout", () => this.handleSocketTimeout(socket));
        socket.on("error", (err) => this.handleSocketError(socket, err));
    }

    /**
     * Get last client message and convert it to string
     */
    private getLastClientMessage(socket: TCPSocket) {
        const lastMessageBuffer = this.clients.get(socket.id!)?.lastMessage;
        return lastMessageBuffer && LogShortener.truncate(`last received message: ${lastMessageBuffer.toString("utf8")}`);
    }

    private logConnections() {
        this.server.getConnections((err, count) => {
            if (err) {
                this.log.error("Error getting count of connections: ", err);
            }
            this.log.info(`Active connection count: ${count}`);
        });
    }
}
