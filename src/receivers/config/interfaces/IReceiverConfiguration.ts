export interface IReceiverConfiguration {
    port: string;
    bufferSizeLimit: number;
    storageTableName: string;
}
