import { IReceiverConfiguration } from "./interfaces/IReceiverConfiguration";

export interface IDPPConfiguration {
    tcp: ITCPDPPConfiguration;
    udp: IUDPDPPConfiguration;
}

export interface ITCPDPPConfiguration {
    tram: IReceiverConfiguration;
    bus: IReceiverConfiguration;
    metro: IReceiverConfiguration;
}

export interface IUDPDPPConfiguration {}

export const DPPConfig: IDPPConfiguration = {
    tcp: {
        tram: {
            port: process.env.DPP_TRAM_RECEIVER_PORT ?? "3000",
            bufferSizeLimit: process.env.DPP_TRAM_BUFFER_SIZE_LIMIT ? parseFloat(process.env.DPP_TRAM_BUFFER_SIZE_LIMIT) : 2,
            storageTableName: process.env.DPP_TRAM_STORAGE_TABLE_NAME ?? "TcpDppTramData",
        },
        bus: {
            port: process.env.DPP_BUS_RECEIVER_PORT ?? "3001",
            bufferSizeLimit: process.env.DPP_BUS_BUFFER_SIZE_LIMIT ? parseFloat(process.env.DPP_BUS_BUFFER_SIZE_LIMIT) : 2,
            storageTableName: process.env.DPP_BUS_STORAGE_TABLE_NAME ?? "TcpDppBusData",
        },
        metro: {
            port: process.env.DPP_METRO_RECEIVER_PORT ?? "3004",
            bufferSizeLimit: process.env.DPP_METRO_BUFFER_SIZE_LIMIT ? parseFloat(process.env.DPP_METRO_BUFFER_SIZE_LIMIT) : 2,
            storageTableName: process.env.DPP_METRO_STORAGE_TABLE_NAME ?? "TcpDppMetroData",
        },
    },
    udp: {},
};
