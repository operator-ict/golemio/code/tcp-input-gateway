export interface IConfiguration {
    nodeEnv: "development" | "test" | "production";
    logLevel: string;
    rabbit: {
        connectionString: string;
        exchangeName: string;
        maxReconnections: number;
        reconnectionTimeout: number;
    };
}

export const Config: IConfiguration = {
    nodeEnv: (process.env.NODE_ENV as IConfiguration["nodeEnv"]) ?? "development",
    logLevel: process.env.LOG_LEVEL ?? "INFO",
    rabbit: {
        connectionString: process.env.RABBIT_CONN ?? "",
        exchangeName: process.env.RABBIT_EXCHANGE_NAME ?? "",
        maxReconnections: process.env.RABBIT_MAX_RECONNECTIONS ? parseInt(process.env.RABBIT_MAX_RECONNECTIONS) : 5,
        reconnectionTimeout: process.env.RABBIT_RECONNECTION_TIMEOUT ? parseInt(process.env.RABBIT_RECONNECTION_TIMEOUT) : 5000,
    },
};
