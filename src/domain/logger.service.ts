import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { createLogger } from "@golemio/core/dist/helpers";
import { DomainToken } from "./constants";
import { IConfiguration } from "./config";
import { ILoggerService, LogObject } from "./interfaces";

@injectable()
export class LoggerService implements ILoggerService {
    private readonly logger: LogObject;

    constructor(@inject(DomainToken.Configuration) config: IConfiguration) {
        this.logger = createLogger({
            projectName: "tcp-input-gateway",
            nodeEnv: config.nodeEnv,
            logLevel: config.logLevel,
        });
    }

    public getLogger() {
        return this.logger;
    }
}
