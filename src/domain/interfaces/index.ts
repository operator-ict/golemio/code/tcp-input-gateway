import { createLogger } from "@golemio/core/dist/helpers";

export interface ILoggerService {
    getLogger(): LogObject;
}

export type LogObject = ReturnType<typeof createLogger>;
export type LogLevel = keyof Pick<LogObject, "error" | "warn" | "info" | "debug" | "silly">;

export interface IAMQPService {
    sendMessageToExchange(key: string, msg: string, options: Record<string, any>): Promise<void>;
    connect(isFailover?: boolean): Promise<void>;
    disconnect(): Promise<Error | null>;
}
