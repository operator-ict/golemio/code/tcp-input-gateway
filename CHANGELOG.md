# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.6.2] - 2024-12-04

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [1.6.1] - 2024-10-09

### Changed

-   Obviously invalid XML messages skip all processing including the XML parser ([tcp-input-gateway#10](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/issues/10))
-   Shorten message logs and enhance error usage ([tcp-input-gateway#10](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/issues/10))

## [1.6.0] - 2024-09-25

### Added

-   Filter out invalid elements from message([pid#319](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/319))

## [1.5.4] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.5.3] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.5.2] - 2024-02-05

### Changed

-   Azure table credentials ([core#88](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/88))

## [1.5.1] - 2024-01-22

-   No changelog

## [1.5.0] - 2023-10-09

### Changed

-   Migrate to Azure table storage ([tcp-ig#4](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/issues/4))

## [1.4.6] - 2023-09-11

### Changed

-   Send ARRIVA City messages to RMQ ([vp#26](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/26))
-   Add message buffer to ARRIVA City TCP receiver
-   Refactoring - ARRIVA Common -> ARRIVA City, TELMAX -> Regional Bus

## [1.4.5] - 2023-07-31

### Changed

-   NodeJS 18.17.0

## [1.4.4] - 2023-07-19

### Added

-   Implement TCP receiver for Arriva common runs ([vp#20](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/20))

### Changed

-   Update TypeScript to v5

## [1.4.3] - 2023-07-17

### Added

-   Open telemetry initialization

## [1.4.1] - 2023-03-22

-   No changelog

## [1.4.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.3.0] - 2023-02-06

### Changed

-   Change storage provider from S3 to Azure blob storage ([tcp-input-gateway#8]([https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/issues/8]))

## [1.2.1] - 2023-01-30

### Changed

-   Change AMQP routing key for tram/bus runs ([pid#176](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/176))

## [1.2.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm
-   Change metro to tcp

## [1.1.2] - 2022-11-29

### Changed

-   Update TypeScript to v4.7.2
-   Replace reflect-metadata with shared reflection library (from the core module)
-   logging done by pino logger

## [1.1.1] - 2022-10-04

## Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))
-   Change message key for DPP metro to `exchangeName.vehiclepositionsruns.saveMetroRunsToDB` ([pid#166](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/166))

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.1.0] - 2022-09-01

### Added

-   Add UDP receiver for DPP metro runs ([pid#165](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/165))

### Changed

-   Refactor existing TCP DPP receivers

## [1.0.3] - 2022-05-04

### Added

-   Factory for TCP DPP receivers
-   Base TCP DPP receiver duplication for trams and busses
-   Prepare base UDP receiver
-   AMQP alternate exchange

