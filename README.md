[![pipeline status](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/badges/master/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/commits/master)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/badges/master/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/commits/master)

# Golemio Data Platform Transport (TCP/UDP) Input Gateway

> TCP and UDP listeners for receiving raw input data

## Local Installation

### Prerequisites

-   Node.js (https://nodejs.org)

### Installation

Install all dependencies using command:

```
npm install
```

in the application's root directory.

### Build & Run

#### Production

To compile TypeScript code into js (production build):

```bash
npm run build-minimal
# Or npm run build to generate source map files
```

To run the app:

```
npm run start
```

#### Dev/debug

Run via TypeScript (in this case it is not needed to build separately, application will watch for changes and restart on save):

```
npm run dev-start
```

or run with a debugger:

```
npm run dev-start-debug
```

Running the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Project uses `dotenv` package: https://www.npmjs.com/package/dotenv

## Available receivers

### DPP Trams

Receive data about DPP (Prague Public Transit Company) trams

-   Protocol: TCP
-   Port: 3000

#### Environmental variables

| Variable                    | Default        |
| --------------------------- | -------------- |
| DPP_TRAM_RECEIVER_PORT      | 3000           |
| DPP_TRAM_BUFFER_SIZE_LIMIT  | 2 (in MB)      |
| DPP_TRAM_STORAGE_TABLE_NAME | TcpDppTramData |

#### Sending data

```bash
npm run send-tcp 3000 \
  '<M><V turnus="17/3" line="17" evc="9168" np="ano" lat="50.05440" lng="14.41812" akt="03030001" takt="2022-08-02T21:56:22" konc="26200002" tjr="2022-08-02T21:56:00" pkt="1885317" tm="2022-08-02T21:56:22" events="O" /></M>'
```

### DPP Buses

Receive data about DPP buses

-   Protocol: TCP
-   Port: 3001

#### Environmental variables

| Variable                   | Default       |
| -------------------------- | ------------- |
| DPP_BUS_RECEIVER_PORT      | 3001          |
| DPP_BUS_BUFFER_SIZE_LIMIT  | 2 (in MB)     |
| DPP_BUS_STORAGE_TABLE_NAME | TcpDppBusData |

#### Sending data

```bash
npm run send-tcp 3001 \
  '<M><V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" /></M>'
```

### DPP Metro

Receive data about DPP metro

-   Protocol: TCP
-   Default port: 3004

#### Environmental variables

| Variable                     | Default         |
| ---------------------------- | --------------- |
| DPP_METRO_RECEIVER_PORT      | 3004            |
| DPP_METRO_BUFFER_SIZE_LIMIT  | 1 (in MB)       |
| DPP_METRO_STORAGE_TABLE_NAME | TcpDppMetroData |

#### Sending data

```bash
npm run send-tcp 3004 \
  '<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"><vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /></m>'
```

### ARRIVA City Bus Data

Receive data about ARRIVA City trips

-   Protocol: TCP
-   Port: 3005

#### Environmental variables

| Variable                       | Default           |
| ------------------------------ | ----------------- |
| ARRIVA_CITY_RECEIVER_PORT      | 3005              |
| ARRIVA_CITY_BUFFER_SIZE_LIMIT  | 2 (in MB)         |
| ARRIVA_CITY_STORAGE_TABLE_NAME | TcpArrivaCityData |

#### Sending data

```bash
npm run send-tcp 3005 \
  '<M><V imei="867377023812625" rz="6AB8491" pkt="3107" lat="50.00201" lng="14.4054" tm="2023-07-02T23:59:44" events="R" rych="11" smer="149" /></M>'
```
