import { container } from "@golemio/core/dist/shared/tsyringe";
import { mockProcessExit } from "jest-mock-process";
import { ReceiverToken } from "#receivers";
import { DomainToken } from "#domain";
import { ITransportInputGatewayApp, ApplicationToken, TransportInputGatewayApp } from "#application";

jest.useFakeTimers("legacy");

/**
 * Describe TransportInputGatewayApp
 */
describe("Application - TransportInputGatewayApp", () => {
    let app: ITransportInputGatewayApp;
    let mockExit: jest.SpyInstance;

    const mocks = {
        startListening: jest.fn(),
        stopListening: jest.fn(),
    };

    const createDependencyContainer = () => {
        app = container
            .createChildContainer()
            .registerSingleton(
                ReceiverToken.Receiver,
                class DummyDPPReceiver {
                    startListening = mocks.startListening;
                    stopListening = mocks.stopListening;
                }
            )
            .registerSingleton(
                DomainToken.AMQPService,
                class DummyAMQPService {
                    connect = jest.fn();
                    disconnect = jest.fn();
                }
            )
            .registerSingleton(ApplicationToken.TransportInputGatewayApp, TransportInputGatewayApp)
            .resolve(ApplicationToken.TransportInputGatewayApp);
    };

    beforeEach(() => {
        mockExit = mockProcessExit();
    });

    afterEach(() => {
        container.clearInstances();
        mockExit.mockRestore();
        for (const key in mocks) {
            mocks[key as keyof typeof mocks].mockClear();
        }
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("receivers should be assigned in the constructor", () => {
            createDependencyContainer();
            expect((app["receivers" as keyof ITransportInputGatewayApp] as any)[0]).toEqual(
                expect.objectContaining({ startListening: expect.any(Function) })
            );
        });
    });

    // =============================================================================
    // start
    // =============================================================================
    describe("start", () => {
        test("start should start all receivers", async () => {
            await app.start();

            expect(mocks.startListening).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // stop
    // =============================================================================
    describe("stop", () => {
        test("stop should stop all receivers (exit code 0)", async () => {
            mocks.stopListening.mockResolvedValueOnce(null);

            app.stop();

            jest.advanceTimersByTime(1000);
            // Flush ongoing promises
            await new Promise((resolve) => setImmediate(resolve));

            expect(mocks.stopListening).toHaveBeenCalledTimes(1);
            expect(mockExit).toBeCalledWith(0);
        });

        test("stop should stop all receivers (exit code 1)", async () => {
            mocks.stopListening.mockResolvedValueOnce(new Error());

            app.stop();

            jest.advanceTimersByTime(1000);
            // Flush ongoing promises
            await new Promise((resolve) => setImmediate(resolve));

            expect(mocks.stopListening).toHaveBeenCalledTimes(1);
            expect(mockExit).toBeCalledWith(1);
        });

        test("stop should stop all receivers (exit code 2)", async () => {
            mocks.stopListening.mockResolvedValueOnce(new Error());

            app.stop();

            jest.advanceTimersByTime(5000);
            expect(mocks.stopListening).toHaveBeenCalledTimes(1);
            expect(mockExit).toBeCalledWith(2);
        });

        test("stop should stop all receivers (custom exit code, no server error)", async () => {
            mocks.stopListening.mockResolvedValueOnce(null);

            app.stop(42);

            jest.advanceTimersByTime(1000);
            // Flush ongoing promises
            await new Promise((resolve) => setImmediate(resolve));

            expect(mocks.stopListening).toHaveBeenCalledTimes(1);
            expect(mockExit).toBeCalledWith(42);
        });

        test("stop should stop all receivers (custom exit code, server error)", async () => {
            mocks.stopListening.mockResolvedValueOnce(new Error());

            app.stop(42);

            jest.advanceTimersByTime(1000);
            // Flush ongoing promises
            await new Promise((resolve) => setImmediate(resolve));

            expect(mocks.stopListening).toHaveBeenCalledTimes(1);
            expect(mockExit).toBeCalledWith(42);
        });
    });
});
