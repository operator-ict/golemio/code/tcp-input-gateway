import { Config, DomainToken, IConfiguration } from "#domain";
import { ArrivaCityReceiver, ReceiverToken } from "#receivers";
import { MessageBuffer } from "#receivers/buffers/message.buffer";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { Socket } from "net";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    sendMessageToExchange: jest.fn(),
    parseData: jest.fn(),
    saveData: jest.fn(),
    getLogger: jest.fn().mockReturnValue(log),
    getValue: jest.fn().mockImplementation((key: string) => {
        switch (key) {
            case "env.ARRIVA_CITY_RECEIVER_PORT":
                return "4242";
            case "env.ARRIVA_CITY_BUFFER_SIZE_LIMIT":
                return "2";
            case "env.ARRIVA_CITY_STORAGE_TABLE_NAME":
                return "TestArrivaCity";
            default:
                return "";
        }
    }),
    byteLength: jest.fn().mockReturnValue(Infinity),
    shouldBeIgnored: jest.fn().mockReturnValue(false),
};

global.Buffer.byteLength = mocks.byteLength;

/**
 * Describe ArrivaCityReceiver
 */
describe("Receivers - ArrivaCityReceiver", () => {
    let receiver: ArrivaCityReceiver;

    const socket = {
        write: jest.fn(),
        destroy: jest.fn(),
        remoteAddress: "172.0.0.1",
        remotePort: "30000",
        readBuffer: new MessageBuffer("</M>"),
    };

    const createDependencyContainer = (rabbitConfig: Partial<IConfiguration["rabbit"]> = {}) => {
        receiver = container
            .createChildContainer()
            .register(DomainToken.Configuration, {
                useValue: { ...Config, rabbit: { ...Config.rabbit, ...rabbitConfig } },
            })
            .register(
                CoreToken.SimpleConfig,
                class DummySimpleConfig {
                    getValue = mocks.getValue;
                }
            )
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(
                DomainToken.AMQPService,
                class DummyAMQPService {
                    sendMessageToExchange = mocks.sendMessageToExchange;
                }
            )
            .register(
                ReceiverToken.StorageManager,
                class DummyStorageManager {
                    saveData = mocks.saveData;
                }
            )
            .registerSingleton(
                ReceiverToken.RegionalBusParser,
                class DummyParser {
                    parseData = mocks.parseData;
                }
            )
            .registerSingleton(
                ReceiverToken.XMLMessageFilter,
                class DummyXMLMessageFilter {
                    shouldBeIgnored = mocks.shouldBeIgnored;
                }
            )
            .registerSingleton(ReceiverToken.Receiver, ArrivaCityReceiver)
            .resolve(ReceiverToken.Receiver);
    };

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            createDependencyContainer();
            expect(receiver["port"]).toEqual("4242");
            expect(receiver["log"]).not.toBeUndefined();
            expect(receiver["server"]).not.toBeUndefined();
        });
    });

    // =============================================================================
    // handleSocketData
    // =============================================================================
    describe("handleSocketData", () => {
        test("handleSocketData should call parent.handleSocketData", async () => {
            createDependencyContainer();
            receiver["logSocketEvent"] = jest.fn();
            await receiver["handleSocketData"](socket as unknown as Socket, Buffer.from("<test>data</test>"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("silly", "Data received", socket, "<test>data</test>");
        });

        test("handleSocketData should send message to queue and store it to our storage", async () => {
            jest.useFakeTimers("modern");
            jest.setSystemTime(1693840054277);

            createDependencyContainer({ exchangeName: "test" });
            mocks.parseData.mockResolvedValueOnce({ test: "data" });
            receiver["logSocketEvent"] = jest.fn();

            await receiver["handleSocketData"](socket as unknown as Socket, Buffer.from("<M>data</M>"));

            expect(mocks.sendMessageToExchange).toHaveBeenCalledTimes(1);
            expect(mocks.sendMessageToExchange).toHaveBeenCalledWith(
                "input-transport.test.vehiclepositionsruns.saveArrivaCityRunsToDB",
                '{"test":"data"}',
                {
                    persistent: true,
                    timestamp: 1693840054277,
                }
            );
            expect(mocks.parseData).toHaveBeenCalledTimes(1);

            jest.useRealTimers();
        });
    });

    // =============================================================================
    // saveDataToStorage
    // =============================================================================
    describe("saveDataToStorage", () => {
        test("saveDataToStorage should save data", async () => {
            createDependencyContainer();
            receiver["saveDataToStorage"]({ M: { V: { $: { attr: "test" } } } }, `<M><V attr="test"</M>`);

            expect(mocks.saveData).toHaveBeenCalledTimes(1);
            expect(mocks.saveData).toHaveBeenCalledWith("TestArrivaCity", [
                {
                    $: {
                        raw: '<M><V attr="test"</M>',
                    },
                },
                {
                    $: {
                        attr: "test",
                    },
                },
            ]);
        });
    });
});
