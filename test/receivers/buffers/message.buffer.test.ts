import { MessageBuffer } from "#receivers/buffers/message.buffer";

/**
 * Describe Message Buffer
 */
describe("Message Buffer", () => {
    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            const readBuffer = new MessageBuffer("\n");

            expect(readBuffer["buffer"]).toEqual("");
            expect(readBuffer["msgEnd"]).toBe("\n");
        });
    });

    // =============================================================================
    // push method
    // =============================================================================
    describe("push", () => {
        test("push should push data to buffer", () => {
            const readBuffer = new MessageBuffer("\n");
            readBuffer.push(Buffer.from("hello"));

            expect(readBuffer["buffer"].toString()).toEqual("hello");
        });
    });

    // =============================================================================
    // isFinished method
    // =============================================================================
    describe("isFinished", () => {
        test("isFinished should should return true if buffer contains message footer (end string)", async () => {
            const readBuffer = new MessageBuffer("\n");
            readBuffer.push(Buffer.from("hello\n"));

            expect(readBuffer.isFinished()).toEqual(true);
        });

        test("isFinished should should return false if buffer does not contain message footer (end string)", async () => {
            const readBuffer = new MessageBuffer("\n");
            readBuffer.push(Buffer.from("hello"));

            expect(readBuffer.isFinished()).toEqual(false);
        });
    });

    // =============================================================================
    // getMessage method
    // =============================================================================
    describe("getMessage", () => {
        test("getMessage should call return specific buffer part", () => {
            const readBuffer = new MessageBuffer("</M>");
            readBuffer.push(Buffer.from("<M>hello</M><M>world</M><M>!"));

            expect(readBuffer.getMessage()).toEqual("<M>hello</M>");
            expect(readBuffer.getMessage()).toEqual("<M>world</M>");
            expect(readBuffer["buffer"]).toEqual("<M>!");
            expect(readBuffer.getMessage()).toBe(null);
        });

        test("getMessage should return null if message is not complete", () => {
            const readBuffer = new MessageBuffer("\n");
            readBuffer.push(Buffer.from("hello world"));

            const msg = readBuffer.getMessage();

            expect(msg).toBe(null);
            expect(readBuffer["buffer"]).toEqual("hello world");
        });
    });
});
