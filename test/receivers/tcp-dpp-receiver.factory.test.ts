import { Config, DomainToken } from "#domain";
import { AbstractDPPReceiver, DPPReceiverEnum, DPPReceiverFactory, IDPPConfiguration, ReceiverToken } from "#receivers";
import { IReceiverConfiguration } from "#receivers/config/interfaces/IReceiverConfiguration";
import { container } from "@golemio/core/dist/shared/tsyringe";

const mocks = {
    getLogger: jest.fn().mockReturnValue({}),
    shouldBeIgnored: jest.fn().mockReturnValue(false),
};

const tramConfig: IReceiverConfiguration = {
    port: "4242",
    bufferSizeLimit: 2,
    storageTableName: "TestTram",
};

const busConfig: IReceiverConfiguration = {
    port: "6969",
    bufferSizeLimit: 2,
    storageTableName: "TestBus",
};

jest.mock("net", () => ({
    ...jest.requireActual("net"),
    createServer: jest.fn(() => {
        return {
            on: jest.fn((_event: string, _callback: Function) => null),
            listen: jest.fn(),
        };
    }),
}));

/**
 * Describe DPPReceiverFactory
 */
describe("Receivers - DPPReceiverFactory", () => {
    const createDependencyContainer = () => {
        container
            .register(DomainToken.Configuration, {
                useValue: { ...Config, rabbit: { ...Config.rabbit, exchangeName: "test" } },
            })
            .register(ReceiverToken.DPPConfiguration, {
                useValue: {
                    tcp: {
                        tram: tramConfig,
                        bus: busConfig,
                    },
                } as IDPPConfiguration,
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(DomainToken.AMQPService, class DummyAMQPService {})
            .register(
                ReceiverToken.StorageManager,
                class DummyStorageManager {
                    bufferAndSaveData = jest.fn();
                    saveData = jest.fn();
                }
            )
            .registerSingleton(
                ReceiverToken.DPPTramBusParser,
                class DummyParser {
                    parseData = jest.fn();
                }
            )
            .registerSingleton(
                ReceiverToken.XMLMessageFilter,
                class DummyXMLMessageFilter {
                    shouldBeIgnored = mocks.shouldBeIgnored;
                }
            );
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // getClass
    // =============================================================================
    describe("getClass", () => {
        test("getClass should return class for trams", async () => {
            const TramClass = DPPReceiverFactory.getClass(DPPReceiverEnum.Tram);
            expect(TramClass.name).toEqual("DPPTramReceiver");

            const receiver = container
                .registerSingleton("DPPTramReceiver", TramClass)
                .resolve<AbstractDPPReceiver>("DPPTramReceiver");

            expect(receiver["port"]).toEqual(tramConfig.port);
            expect(receiver["receiverConfig"]).toStrictEqual(tramConfig);
            expect(receiver["amqpMessageKey"]).toEqual(`input-transport.test.vehiclepositionsruns.saveTramRunsToDB`);
        });

        test("getClass should return class for busses", async () => {
            const BusClass = DPPReceiverFactory.getClass(DPPReceiverEnum.Bus);
            expect(BusClass.name).toEqual("DPPBusReceiver");

            const receiver = container
                .registerSingleton("DPPBusReceiver", BusClass)
                .resolve<AbstractDPPReceiver>("DPPBusReceiver");
            expect(receiver["port"]).toEqual(busConfig.port);
            expect(receiver["receiverConfig"]).toStrictEqual(busConfig);
            expect(receiver["amqpMessageKey"]).toEqual(`input-transport.test.vehiclepositionsruns.saveBusRunsToDB`);
        });
    });
});
