import { container } from "@golemio/core/dist/shared/tsyringe";
import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { StorageManager } from "#receivers/managers";

const log = {
    silly: jest.fn(),
    verbose: jest.fn(),
    error: jest.fn(),
};

const mocks = {
    createEntities: jest.fn(),
    getLogger: jest.fn().mockReturnValue(log),
};

/**
 * Describe StorageManager
 */
describe("Receiver Managers - StorageManager", () => {
    let manager: StorageManager;

    const createDependencyContainer = () => {
        manager = container
            .registerSingleton(
                DomainToken.TableStorageService,
                class DummyStorageService {
                    createEntities = mocks.createEntities;
                }
            )
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(ReceiverToken.StorageManager, StorageManager)
            .resolve(ReceiverToken.StorageManager);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // saveData
    // =============================================================================
    describe("saveData", () => {
        test("should call createEntities", async () => {
            const tableName = "test-table";
            const inputData = [{ $: { id: 1 } }, { $: { id: 2 } }];
            const messageData = { message: "test" };

            await manager.saveData(tableName, inputData, messageData);
            expect(mocks.createEntities).toBeCalledWith(tableName, [
                { id: 1, message: "test" },
                { id: 2, message: "test" },
            ]);
        });
    });
});
