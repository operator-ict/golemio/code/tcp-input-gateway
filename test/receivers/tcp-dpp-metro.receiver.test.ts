import { Config, DomainToken, IConfiguration } from "#domain";
import { DPPMetroReceiver, IDPPConfiguration, ReceiverToken } from "#receivers";
import { MessageBuffer } from "#receivers/buffers/message.buffer";
import { IReceiverConfiguration } from "#receivers/config/interfaces/IReceiverConfiguration";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { Socket } from "net";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    sendMessageToExchange: jest.fn(),
    parseData: jest.fn(),
    bufferAndSaveData: jest.fn(),
    saveData: jest.fn(),
    getLogger: jest.fn().mockReturnValue(log),
    byteLength: jest.fn().mockReturnValue(Infinity),
    shouldBeIgnored: jest.fn().mockReturnValue(false),
};

const metroConfig: IReceiverConfiguration = {
    port: "4242",
    bufferSizeLimit: 2,
    storageTableName: "TestMetro",
};

global.Buffer.byteLength = mocks.byteLength;

/**
 * Describe DPPMetroReceiver
 */
describe("Receivers - DPPMetroReceiver", () => {
    let receiver: DPPMetroReceiver;

    const socket = {
        write: jest.fn(),
        destroy: jest.fn(),
        remoteAddress: "172.0.0.1",
        remotePort: "30000",
        readBuffer: new MessageBuffer("</m>"),
    };

    const createDependencyContainer = (rabbitConfig: Partial<IConfiguration["rabbit"]> = {}) => {
        receiver = container
            .createChildContainer()
            .register(DomainToken.Configuration, {
                useValue: { ...Config, rabbit: { ...Config.rabbit, ...rabbitConfig } },
            })
            .register(ReceiverToken.DPPConfiguration, {
                useValue: {
                    tcp: {
                        metro: metroConfig,
                    },
                } as IDPPConfiguration,
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(
                DomainToken.AMQPService,
                class DummyAMQPService {
                    sendMessageToExchange = mocks.sendMessageToExchange;
                }
            )
            .register(
                ReceiverToken.StorageManager,
                class DummyStorageManager {
                    bufferAndSaveData = mocks.bufferAndSaveData;
                    saveData = mocks.saveData;
                }
            )
            .registerSingleton(
                ReceiverToken.DPPMetroParser,
                class DummyParser {
                    parseData = mocks.parseData;
                }
            )
            .registerSingleton(
                ReceiverToken.XMLMessageFilter,
                class DummyXMLMessageFilter {
                    shouldBeIgnored = mocks.shouldBeIgnored;
                }
            )
            .registerSingleton(ReceiverToken.Receiver, DPPMetroReceiver)
            .resolve(ReceiverToken.Receiver);
    };

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            createDependencyContainer();
            expect(receiver["name"]).toEqual("DPPMetroReceiver");
            expect(receiver["port"]).toEqual("4242");
            expect(receiver["log"]).not.toBeUndefined();
            expect(receiver["server"]).not.toBeUndefined();
        });
    });

    // =============================================================================
    // handleSocketMessage
    // =============================================================================
    describe("handleSocketData", () => {
        test("handleSocketMessage should call parent.handleSocketMessage", async () => {
            createDependencyContainer();
            receiver["logSocketEvent"] = jest.fn();
            await receiver["handleSocketData"](socket as unknown as Socket, Buffer.from("<test>data</test>"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("silly", "Data received", socket, "<test>data</test>");
        });

        test("handleSocketMessage should send message to queue and store it to our storage", async () => {
            createDependencyContainer({ exchangeName: "test" });
            mocks.parseData.mockResolvedValueOnce({ test: "data" });
            receiver["logSocketEvent"] = jest.fn();
            jest.spyOn(Date, "now").mockImplementation(() => 1623680638000);
            await receiver["handleSocketData"](socket as unknown as Socket, Buffer.from("<m>data</m>"));

            expect(mocks.sendMessageToExchange).toHaveBeenCalledTimes(1);
            expect(mocks.sendMessageToExchange).toHaveBeenCalledWith(
                "input-transport.test.vehiclepositionsruns.saveMetroRunsToDB",
                '{"test":"data"}',
                {
                    persistent: true,
                    timestamp: 1623680638000,
                }
            );
            expect(mocks.parseData).toHaveBeenCalledTimes(1);
        });
    });
});
