import udp, { RemoteInfo } from "dgram";
import { LogObject } from "#domain";
import { AbstractUDPReceiver } from "#receivers/udp-abstract.receiver";

const disconnectMock = jest.fn();
const closeMock = jest.fn((callback) => callback());
jest.mock("dgram", () => ({
    ...jest.requireActual("dgram"),
    createSocket: jest.fn(() => {
        return {
            on: jest.fn(),
            bind: jest.fn(),
            disconnect: disconnectMock,
            close: closeMock,
        };
    }),
}));

// This is necessary as the AbstractUDPReceiver class is abstract
class UDPReceiver extends AbstractUDPReceiver {}

/**
 * Describe AbstractUDPReceiver
 */
describe("Receivers - AbstractUDPReceiver", () => {
    const log = {
        info: jest.fn(),
        error: jest.fn(),
    };

    const rinfo: Partial<RemoteInfo> = {
        address: "127.0.0.1",
        port: 4200,
    };

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            const receiver = new UDPReceiver("3000", 42, log as unknown as LogObject);

            expect(receiver["port"]).toEqual("3000");
            expect(receiver["bufferSizeLimit"]).toEqual(42);
            expect(receiver["log"]).toStrictEqual(log);
            expect(receiver["server"]).not.toBeUndefined();
            expect(udp.createSocket).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // startListening
    // =============================================================================
    describe("startListening", () => {
        test("startListening should call listen on Server", () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            receiver.startListening();

            expect(receiver["server"].bind).toHaveBeenCalledWith({ port: 3000 });
        });
    });

    // =============================================================================
    // stopListening
    // =============================================================================
    describe("stopListening", () => {
        test("stopListening should disconnect and close socket", async () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            const promise = await receiver.stopListening();

            expect(receiver["server"].disconnect).toHaveBeenCalledTimes(1);
            expect(receiver["server"].close).toHaveBeenCalledTimes(1);
            expect(promise).toBeNull();
        });

        test("stopListening should resolve with an error (disconnect)", async () => {
            disconnectMock.mockImplementationOnce(() => {
                throw new Error("test");
            });

            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            const promise = receiver.stopListening();

            await expect(promise).resolves.toBeInstanceOf(Error);
            expect(log.error).toHaveBeenCalledWith("[UDPReceiver] Error while disconnecting from remote", expect.any(Error));
        });
    });

    // =============================================================================
    // logSocketEvent
    // =============================================================================
    describe("logSocketEvent", () => {
        test("logSocketEvent should call the logger with specific parameters", () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            receiver["logSocketEvent"]("info", "test", rinfo as RemoteInfo);

            expect(log.info).toHaveBeenCalledWith({
                receiver: "UDPReceiver",
                eventMessage: "test",
                remoteAddress: rinfo.address,
                remotePort: rinfo.port,
            });
        });

        test("logSocketEvent should call the logger with specific parameters (optional data)", () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            receiver["logSocketEvent"]("info", "test", rinfo as RemoteInfo, "test");

            expect(log.info).toHaveBeenCalledWith({
                receiver: "UDPReceiver",
                eventMessage: "test",
                remoteAddress: rinfo.address,
                remotePort: rinfo.port,
                data: "test",
            });
        });
    });

    // =============================================================================
    // handleSocketMessage
    // =============================================================================
    describe("handleSocketMessage", () => {
        test("handleSocketMessage should log ack info", () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketMessage"](rinfo as RemoteInfo, Buffer.from("test"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("silly", "Data received", rinfo, "test");
        });

        test("handleSocketMessage should reject data and log ack info", () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketMessage"]({ ...rinfo, size: 2 * 1024 * 1024 } as RemoteInfo, Buffer.from("test"));
            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("error", "Data size limit exceeded", {
                ...rinfo,
                size: 2 * 1024 * 1024,
            });
        });
    });

    // =============================================================================
    // handleSocketError
    // =============================================================================
    describe("handleSocketError", () => {
        test("handleSocketError should log ack info", () => {
            const receiver = new UDPReceiver("3000", 1, log as unknown as LogObject);
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketError"](new Error("Some error"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("error", "Connection failure: Some error");
        });
    });
});
