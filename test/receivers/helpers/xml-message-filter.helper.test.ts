import { XMLMessageFilter } from "#receivers/helpers/xml-message-filter.helper";

/**
 * Describe XMLMessageFilter
 */
describe("Receiver Helpers - XMLMessageFilter", () => {
    // =============================================================================
    // shouldBeIgnored
    // =============================================================================
    describe("shouldBeIgnored", () => {
        test("should ignore an obvious non-XML message", () => {
            const input = "Lorem ipsum dolor sit amet.";
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(true);
        });

        test("should ignore an empty message", () => {
            const input = "";
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(true);
        });

        test("should ignore a message containing only whitespace characters", () => {
            const input = "\t\r\n  ";
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(true);
        });

        test("should not ignore a valid XML message with a single pair tag", () => {
            const input = "<M></M>";
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });

        test("should not ignore a valid XML message with a single self-closing tag", () => {
            const input = "<M />";
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });

        test("should not ignore a valid XML message with a single pair tag surrounded by whitespace", () => {
            const input = "  <M></M>  ";
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });

        test("should not ignore a valid XML message with DPP tram data", () => {
            // eslint-disable-next-line max-len
            const input = `<M><V turnus="17/3" line="17" evc="9168" np="ano" lat="50.05440" lng="14.41812" akt="03030001" takt="2022-08-02T21:56:22" konc="26200002" tjr="2022-08-02T21:56:00" pkt="1885317" tm="2022-08-02T21:56:22" events="O" /></M>`;
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });

        test("should not ignore a valid XML message with DPP bus data", () => {
            // eslint-disable-next-line max-len
            const input = `<M><V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" /></M>`;
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });

        test("should not ignore a valid XML message with DPP metro data", () => {
            // eslint-disable-next-line max-len
            const input = `<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"><vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /></m>`;
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });

        test("should not ignore a valid XML message with Arriva City bus data", () => {
            // eslint-disable-next-line max-len
            const input = `<M><V imei="867377023812625" rz="6AB8491" pkt="3107" lat="50.00201" lng="14.4054" tm="2023-07-02T23:59:44" events="R" rych="11" smer="149" /></M>`;
            const result = new XMLMessageFilter().shouldBeIgnored(input);
            expect(result).toEqual(false);
        });
    });
});
