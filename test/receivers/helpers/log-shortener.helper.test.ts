import { LogShortener } from "#receivers/helpers/log-shortener.helper";

/**
 * Describe LogShortener
 */
describe("Receiver Helpers - LogShortener", () => {
    // =============================================================================
    // truncate
    // =============================================================================
    describe("truncate", () => {
        test("should shorten a message that is of length that is higher than default max length", () => {
            const input = "".padEnd(131, ".");
            const result = LogShortener.truncate(input);
            expect(result).toEqual(`${"".padEnd(115, ".")}${"... (truncated)"}`);
        });

        test("should not shorten a message that is of length that is less than or equal to default max length", () => {
            const input = "".padEnd(130, ".");
            const result = LogShortener.truncate(input);
            expect(result).toEqual(input);
        });

        test("should not shorten an empty message", () => {
            const input = "";
            const result = LogShortener.truncate(input);
            expect(result).toEqual(input);
        });

        test("should shorten a message that is of length that is higher than a given max length", () => {
            const input = "".padEnd(100, ".");
            const result = LogShortener.truncate(input, 30);
            expect(result).toEqual(`${"".padEnd(15, ".")}${"... (truncated)"}`);
        });

        test("should appropriately handle very small max length values", () => {
            const input = "".padEnd(100, ".");
            const result = LogShortener.truncate(input, 5);
            expect(result).toEqual("... (truncated)");
        });
    });
});
