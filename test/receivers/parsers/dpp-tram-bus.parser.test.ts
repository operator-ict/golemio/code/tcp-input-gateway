import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { DPPTramBusParser } from "#receivers/parsers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    getLogger: jest.fn().mockReturnValue(log),
};

/**
 * Describe DPPTramBusParser
 */
describe("Receiver Parsers - DPPTramBusParser", () => {
    let parser: DPPTramBusParser;

    const createDependencyContainer = () => {
        parser = container
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(ReceiverToken.DPPTramBusParser, DPPTramBusParser)
            .resolve(ReceiverToken.DPPTramBusParser);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // parseData
    // =============================================================================
    describe("parseData", () => {
        test("parseData should parse valid xml", async () => {
            const result = await parser.parseData(
                // eslint-disable-next-line max-len
                '<M><V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" /></M>'
            );

            expect(result).toEqual({
                M: {
                    V: {
                        $: {
                            turnus: "134/3",
                            line: "134",
                            evc: "3738",
                            np: "ano",
                            lat: "50.03952",
                            lng: "14.42919",
                            akt: "07960001",
                            takt: "2022-06-29T17:28:13",
                            konc: "01100005",
                            tjr: "2022-06-29T17:23:00",
                            pkt: "12709236",
                            tm: "2022-06-29T17:28:22",
                            events: "O",
                        },
                    },
                },
            });
            expect(log.error).toHaveBeenCalledTimes(0);
        });

        test("parseData should try to recover data if xml is not valid", async () => {
            const input =
                // eslint-disable-next-line max-len
                '<M><V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" />';
            const result = await parser.parseData(input);

            expect(result).toEqual({
                M: {
                    V: {
                        $: {
                            turnus: "134/3",
                            line: "134",
                            evc: "3738",
                            np: "ano",
                            lat: "50.03952",
                            lng: "14.42919",
                            akt: "07960001",
                            takt: "2022-06-29T17:28:13",
                            konc: "01100005",
                            tjr: "2022-06-29T17:23:00",
                            pkt: "12709236",
                            tm: "2022-06-29T17:28:22",
                            events: "O",
                        },
                    },
                },
            });
            const error = log.error.mock.calls[0][0];
            expect(error).toBeInstanceOf(GeneralError);
            expect(error.message).toMatch(
                /Error parsing received data. Attempting recovery from: <M><V turnus="134\/3".+\.{3} \(truncated\)/
            );
            expect(error.className).toBe("DPPTramBusParser");
            expect(error.info).toBeInstanceOf(Error);
            expect(log.warn).toHaveBeenCalledTimes(0);
        });

        test("parseData should resolve and log error if xml is not valid", async () => {
            const parsePromise = parser.parseData("not xml");

            await expect(parsePromise).resolves.toBeUndefined();
            const error = log.error.mock.calls[0][0];
            expect(error).toBeInstanceOf(GeneralError);
            expect(error.message).toMatch("Error parsing received data. Attempting recovery from: not xml");
            expect(error.className).toBe("DPPTramBusParser");
            expect(error.info).toBeInstanceOf(Error);
            expect(log.warn).toHaveBeenCalledWith("[DPPTramBusParser] Could not recover partial data");
        });

        test("Filter out invalid elements from message", async () => {
            // const input =
            // eslint-disable-next-line max-len
            //     '<M><V turnus="134/3" line="134" evc="3738" np="ano" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" /></M>';

            const arrInput =
                // eslint-disable-next-line max-len
                '<M> <V turnus="134/3" line="134" evc="3738" np="ano" akt="07960001" lat="50.03952" lng="14.42919" takt="2022-06-40" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" /><V turnus="135/4" line="135" evc="3740" np="ano" akt="07960002" lat="50.03952" lng="14.42919" takt="2022-06-29T18:00:00" konc="01100006" tjr="2022-06-29T17:55:00" pkt="12709237" tm="2022-06-29T18:00:10" events="P" /> <V turnus="136/5" line="136" evc="3741" np="ano" akt="07960003" takt="2022-06-29T18:30:00" konc="01100007" tjr="2022-06-29T18:25:00" pkt="12709238" tm="2022-06-29T18:30:15" events="Q" /><V turnus="137/6" line="137" evc="3742" np="ano" akt="07960004" takt="2022-06-29T19:00:00" konc="01100008" tjr="2022-06-29T18:55:00" pkt="12709239" tm="2022-06-29T19:00:20" events="R" /></M>';

            const result = await parser.parseData(arrInput);

            expect(result).toEqual({
                M: {
                    V: [
                        {
                            $: {
                                turnus: "135/4",
                                line: "135",
                                evc: "3740",
                                np: "ano",
                                lat: "50.03952",
                                lng: "14.42919",
                                akt: "07960002",
                                takt: "2022-06-29T18:00:00",
                                konc: "01100006",
                                tjr: "2022-06-29T17:55:00",
                                pkt: "12709237",
                                tm: "2022-06-29T18:00:10",
                                events: "P",
                            },
                        },
                    ],
                },
            });
        });
    });
});
