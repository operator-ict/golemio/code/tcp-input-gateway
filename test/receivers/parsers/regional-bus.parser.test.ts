import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { RegionalBusParser } from "#receivers/parsers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    getLogger: jest.fn().mockReturnValue(log),
};

/**
 * Describe RegionalBusParser
 */
describe("Receiver Parsers - RegionalBusParser", () => {
    let parser: RegionalBusParser;

    const createDependencyContainer = () => {
        parser = container
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(ReceiverToken.RegionalBusParser, RegionalBusParser)
            .resolve(ReceiverToken.RegionalBusParser);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // parseData
    // =============================================================================
    describe("parseData", () => {
        test("parseData should parse valid xml", async () => {
            const result = await parser.parseData(
                // eslint-disable-next-line max-len
                '<M><V imei="867377023812625" rz="6AB8491" pkt="3107" lat="50.00201" lng="14.4054" tm="2023-07-02T23:59:44" events="R" rych="11" smer="149" /></M>'
            );

            expect(result).toEqual({
                M: {
                    V: {
                        $: {
                            imei: "867377023812625",
                            rz: "6AB8491",
                            pkt: "3107",
                            lat: "50.00201",
                            lng: "14.4054",
                            tm: "2023-07-02T23:59:44",
                            events: "R",
                            rych: "11",
                            smer: "149",
                        },
                    },
                },
            });
            expect(log.error).toHaveBeenCalledTimes(0);
        });

        test("parseData should try to recover data if xml is not valid", async () => {
            const input =
                // eslint-disable-next-line max-len
                '<M><V imei="867377023812625" rz="6AB8491" pkt="3107" lat="50.00201" lng="14.4054" tm="2023-07-02T23:59:44" events="R" rych="11" smer="149" /><V imei=';
            const result = await parser.parseData(input);

            expect(result).toEqual({
                M: {
                    V: {
                        $: {
                            imei: "867377023812625",
                            rz: "6AB8491",
                            pkt: "3107",
                            lat: "50.00201",
                            lng: "14.4054",
                            tm: "2023-07-02T23:59:44",
                            events: "R",
                            rych: "11",
                            smer: "149",
                        },
                    },
                },
            });
            const error = log.error.mock.calls[0][0];
            expect(error).toBeInstanceOf(GeneralError);
            expect(error.message).toMatch(
                /Error parsing received data\. Attempting recovery from: <M><V imei="867377023812625".+\.{3} \(truncated\)/
            );
            expect(error.className).toBe("RegionalBusParser");
            expect(error.info).toBeInstanceOf(Error);
            expect(log.warn).toHaveBeenCalledTimes(0);
        });

        test("parseData should resolve and log error if xml is not valid", async () => {
            const parsePromise = parser.parseData("not xml");

            await expect(parsePromise).resolves.toBeUndefined();
            const error = log.error.mock.calls[0][0];
            expect(error).toBeInstanceOf(GeneralError);
            expect(error.message).toMatch("Error parsing received data. Attempting recovery from: not xml");
            expect(error.className).toBe("RegionalBusParser");
            expect(error.info).toBeInstanceOf(Error);
        });
    });
});
