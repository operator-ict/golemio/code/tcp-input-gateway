import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { DPPMetroParser } from "#receivers/parsers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    getLogger: jest.fn().mockReturnValue(log),
};

/**
 * Describe DPPMetroParser
 */
describe("Receiver Parsers - DPPMetroParser", () => {
    let parser: DPPMetroParser;

    const createDependencyContainer = () => {
        parser = container
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(ReceiverToken.DPPMetroParser, DPPMetroParser)
            .resolve(ReceiverToken.DPPMetroParser);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // parseData
    // =============================================================================
    describe("parseData", () => {
        test("parseData should parse valid xml", async () => {
            const result = await parser.parseData(
                '<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"><vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /></m>'
            );

            expect(result).toEqual({
                m: {
                    $: { linka: "A", tm: "2022-07-20T13:45:34Z", gvd: "GD20a" },
                    vlak: { $: { csp: " 3", csr: " 3", cv: "279", ko: "1809", odch: "25" } },
                },
            });
            expect(log.error).toHaveBeenCalledTimes(0);
        });

        test("parseData should try to recover data if xml is not valid", async () => {
            const input =
                // eslint-disable-next-line max-len
                '<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"><vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /><vlak csp';
            const result = await parser.parseData(input);

            expect(result).toEqual({
                m: {
                    $: { linka: "A", tm: "2022-07-20T13:45:34Z", gvd: "GD20a" },
                    vlak: { $: { csp: " 3", csr: " 3", cv: "279", ko: "1809", odch: "25" } },
                },
            });
            const error = log.error.mock.calls[0][0];
            expect(error).toBeInstanceOf(GeneralError);
            expect(error.message).toMatch(
                // eslint-disable-next-line max-len
                /Error parsing received data\. Attempting recovery from: <m linka="A" tm="2022-07-20T13:45:34Z".+\.{3} \(truncated\)/
            );
            expect(error.className).toBe("DPPMetroParser");
            expect(error.info).toBeInstanceOf(Error);
            expect(log.warn).toHaveBeenCalledTimes(0);
        });

        test("parseData should resolve and log error if xml is not valid", async () => {
            const parsePromise = parser.parseData("not xml");

            await expect(parsePromise).resolves.toBeUndefined();
            const error = log.error.mock.calls[0][0];
            expect(error).toBeInstanceOf(GeneralError);
            expect(error.message).toMatch("Error parsing received data. Attempting recovery from: not xml");
            expect(error.className).toBe("DPPMetroParser");
            expect(error.info).toBeInstanceOf(Error);
            expect(log.warn).toHaveBeenCalledWith("[DPPMetroParser] Could not recover partial data");
        });
    });
});
