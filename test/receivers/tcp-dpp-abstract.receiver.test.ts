import { Config, DomainToken, IConfiguration } from "#domain";
import { AbstractDPPReceiver, ReceiverToken } from "#receivers";
import { MessageBuffer } from "#receivers/buffers/message.buffer";
import { container, inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { Socket } from "net";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    sendMessageToExchange: jest.fn(),
    parseData: jest.fn(),
    bufferAndSaveData: jest.fn(),
    saveData: jest.fn(),
    getLogger: jest.fn().mockReturnValue(log),
    byteLength: jest.fn().mockReturnValue(Infinity),
    shouldBeIgnored: jest.fn().mockReturnValue(false),
};

global.Buffer.byteLength = mocks.byteLength;

// This is necessary as the AbstractDPPReceiver class is abstract
@injectable()
class DPPReceiver extends AbstractDPPReceiver {
    protected readonly amqpService;
    protected readonly storageManager;
    protected readonly messageFilter;
    protected readonly messageParser;
    protected readonly receiverConfig;
    protected readonly amqpMessageKey;

    constructor(
        @inject(DomainToken.LoggerService) loggerService: any,
        @inject(DomainToken.AMQPService) amqpService: any,
        @inject(ReceiverToken.StorageManager) storageManager: any,
        @inject(ReceiverToken.XMLMessageFilter) messageFilter: any,
        @inject(ReceiverToken.DPPTramBusParser) messageParser: any
    ) {
        super("4242", 2, loggerService.getLogger());

        this.amqpService = amqpService;
        this.storageManager = storageManager;
        this.messageFilter = messageFilter;
        this.messageParser = messageParser;

        this.receiverConfig = {
            port: "4242",
            bufferSizeLimit: 2,
            storageTableName: "TcpDppTest",
        };
        this.amqpMessageKey = "input-transport.test.test";
    }

    protected saveDataToStorage(payload: object, message: string): void {}
}

/**
 * Describe AbstractDPPReceiver
 */
describe("Receivers - AbstractDPPReceiver", () => {
    let receiver: DPPReceiver;
    const socket = {
        write: jest.fn(),
        destroy: jest.fn(),
        remoteAddress: "172.0.0.1",
        remotePort: "30000",
        readBuffer: new MessageBuffer("</M>"),
    };

    const createDependencyContainer = (rabbitConfig: Partial<IConfiguration["rabbit"]> = {}) => {
        receiver = container
            .createChildContainer()
            .register(DomainToken.Configuration, {
                useValue: { ...Config, rabbit: { ...Config.rabbit, ...rabbitConfig } },
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(
                DomainToken.AMQPService,
                class DummyAMQPService {
                    sendMessageToExchange = mocks.sendMessageToExchange;
                }
            )
            .register(
                ReceiverToken.StorageManager,
                class DummyStorageManager {
                    bufferAndSaveData = mocks.bufferAndSaveData;
                    saveData = mocks.saveData;
                }
            )
            .registerSingleton(
                ReceiverToken.DPPTramBusParser,
                class DummyParser {
                    parseData = mocks.parseData;
                }
            )
            .registerSingleton(
                ReceiverToken.XMLMessageFilter,
                class DummyXMLMessageFilter {
                    shouldBeIgnored = mocks.shouldBeIgnored;
                }
            )
            .registerSingleton(ReceiverToken.Receiver, DPPReceiver)
            .resolve(ReceiverToken.Receiver);
    };

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            createDependencyContainer();
            expect(receiver["port"]).toEqual("4242");
            expect(receiver["log"]).not.toBeUndefined();
            expect(receiver["server"]).not.toBeUndefined();
        });
    });

    // =============================================================================
    // handleSocketData
    // =============================================================================
    describe("handleSocketData", () => {
        test("handleSocketData should call parent.handleSocketData", async () => {
            createDependencyContainer();
            receiver["logSocketEvent"] = jest.fn();
            await receiver["handleSocketData"](socket as unknown as Socket, Buffer.from("<test>data</test>"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("silly", "Data received", socket, "<test>data</test>");
        });

        test("handleSocketData should send message to queue and store it to our storage", async () => {
            createDependencyContainer({ exchangeName: "test" });
            mocks.parseData.mockResolvedValueOnce({ test: "data" });
            receiver["logSocketEvent"] = jest.fn();
            jest.spyOn(Date, "now").mockImplementation(() => 1623680638000);
            await receiver["handleSocketData"](socket as unknown as Socket, Buffer.from("<M>data</M>"));

            expect(mocks.sendMessageToExchange).toHaveBeenCalledTimes(1);
            expect(mocks.sendMessageToExchange).toHaveBeenCalledWith("input-transport.test.test", '{"test":"data"}', {
                persistent: true,
                timestamp: 1623680638000,
            });
            expect(mocks.parseData).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // handleSocketTimeout
    // =============================================================================
    describe("handleSocketTimeout", () => {
        test("handleSocketTimeout should call parent.handleSocketTimeout", async () => {
            createDependencyContainer();
            receiver["logSocketEvent"] = jest.fn();
            await receiver["handleSocketTimeout"](socket as unknown as Socket);

            expect(socket.destroy).toHaveBeenCalled();
            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("warn", "Connection timed out", socket, undefined);
        });

        test("handleSocketTimeout should process readBuffer data", async () => {
            createDependencyContainer({ exchangeName: "test" });
            mocks.parseData.mockResolvedValueOnce({ M: { V: { $: { atr: "value" } } } });
            receiver["logSocketEvent"] = jest.fn();

            jest.spyOn(Date, "now").mockImplementation(() => 1623680638000);

            socket.readBuffer.push(Buffer.from('<M><V atr="value"/><V atr='));

            await receiver["handleSocketTimeout"](socket as unknown as Socket);

            expect(mocks.sendMessageToExchange).toHaveBeenCalledTimes(1);
            expect(mocks.sendMessageToExchange).toHaveBeenCalledWith(
                "input-transport.test.test",
                '{"M":{"V":{"$":{"atr":"value"}}}}',
                {
                    persistent: true,
                    timestamp: 1623680638000,
                }
            );
        });
    });
});
