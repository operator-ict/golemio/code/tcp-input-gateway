import { createLogger } from "@golemio/core/dist/helpers";
import { Config } from "#domain/config";
import { LoggerService } from "#domain/logger.service";

jest.mock("@golemio/core/dist/helpers", () => {
    return { createLogger: jest.fn().mockReturnValue({ mockLogger: "TEST" }) };
});

/**
 * Describe LoggerService
 */
describe("Domain - LoggerService", () => {
    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("createLogger should be called with specific parameters in the constructor", () => {
            new LoggerService(Config);
            expect(createLogger).toBeCalledWith({
                projectName: "tcp-input-gateway",
                nodeEnv: "test",
                logLevel: "INFO",
            });
        });
    });

    // =============================================================================
    // getLogger
    // =============================================================================
    describe("getLogger", () => {
        test("getLogger should return a logger", () => {
            const service = new LoggerService(Config);
            expect(service.getLogger()).toHaveProperty("mockLogger", "TEST");
        });
    });
});
