import { container } from "@golemio/core/dist/shared/tsyringe";
import { IConfiguration, IAMQPService, DomainToken, Config } from "#domain";
import { AMQPService } from "#domain/amqp.service";

const mocks = {
    createChannel: jest.fn().mockReturnValue({
        assertExchange: jest.fn(),
        publish: jest.fn(),
        close: jest.fn(),
        on: jest.fn(),
    }),
    connectionEvent: jest.fn(),
    close: jest.fn(),
    getLogger: jest.fn().mockReturnValue({
        error: jest.fn(),
        warn: jest.fn(),
        info: jest.fn(),
    }),
};

jest.mock("amqplib", () => {
    return {
        connect: jest.fn(() => ({
            createChannel: mocks.createChannel,
            on: mocks.connectionEvent,
            close: mocks.close,
        })),
    };
});

/**
 * Describe AMQPService
 */
describe("Domain - AMQPService", () => {
    let service: IAMQPService;

    const createDependencyContainer = (rabbitConfig: Partial<IConfiguration["rabbit"]> = {}) => {
        service = container
            .createChildContainer()
            .register(DomainToken.Configuration, {
                useValue: {
                    ...Config,
                    rabbit: { ...Config.rabbit, ...rabbitConfig, reconnectionTimeout: 100, maxReconnections: 1 },
                },
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(DomainToken.AMQPService, AMQPService)
            .resolve(DomainToken.AMQPService);
    };

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("constructor should call getLogger", () => {
            createDependencyContainer();
            expect(mocks.getLogger).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // connect
    // =============================================================================
    describe("connect", () => {
        test("connect should throw an exception", async () => {
            createDependencyContainer();
            await expect(service.connect()).rejects.toThrowError("[AMQP] The ENV variable RABBIT_CONN cannot be undefined");
        });

        test("connect should resolve", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });
            await expect(service.connect()).resolves.toBeUndefined();
        });

        test("connect should create container and channel", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });
            await service.connect();

            expect(service["connection" as keyof IAMQPService]).toEqual(
                expect.objectContaining({ createChannel: expect.any(Function) })
            );

            expect(service["channel" as keyof IAMQPService]).toEqual(
                expect.objectContaining({
                    assertExchange: expect.any(Function),
                })
            );

            expect(mocks.createChannel).toHaveBeenCalledTimes(1);
            expect(mocks.connectionEvent).toHaveBeenCalledTimes(2);
        });

        test("connect should try reconnecting", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });

            await service.connect(true);
            expect((service as any)["numberOfReconnectionAttempts"]).toEqual(1);
        });

        test("connect should NOT try reconnecting", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });

            await service.connect();
            expect((service as any)["numberOfReconnectionAttempts"]).toEqual(0);
        });
    });

    // =============================================================================
    // disconnect
    // =============================================================================
    describe("disconnect", () => {
        test("connect should resolve (close not called)", async () => {
            createDependencyContainer();
            await expect(service.disconnect()).resolves.toBeNull();
            expect(mocks.close).not.toHaveBeenCalled();
        });

        test("connect should resolve (close called)", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });
            await service.connect();
            await expect(service.disconnect()).resolves.toBeNull();
            expect(mocks.close).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // sendMessageToExchange
    // =============================================================================
    describe("sendMessageToExchange", () => {
        test("sendMessageToExchange should resolve and log error", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });
            await expect(service.sendMessageToExchange("test", "test", {})).resolves.toBeUndefined();
            expect(mocks.getLogger().error).toHaveBeenCalledWith("[AMQP] Cannot send the message to exchange", expect.any(Error));
        });

        test("sendMessageToExchange should resolve without error", async () => {
            createDependencyContainer({ connectionString: "amqp://test:test@test:5672" });
            await service.connect();
            await expect(service.sendMessageToExchange("test", "test", {})).resolves.toBeUndefined();
        });
    });
});
